if node['lvm_merged']
  package "lvm2"
  execute 'setup lvm for /tmp and /var/lib/lxc' do
    not_if 'grep -q /var/lib/lxc /etc/fstab'
    commands = [
      'vgcreate vg_lxc %s' % node['lvm_merged']['pvs'].join(' '),
      'modprobe dm-mod',
      'lvcreate --type striped --name lxc-containers --extents 100%VG vg_lxc',
      'mkfs.ext4 /dev/vg_lxc/lxc-containers',
      'mkdir -p /mnt/lxc-containers',
      'mount /dev/vg_lxc/lxc-containers /mnt/lxc-containers',
      'mkdir -p /mnt/lxc-containers/tmp',
      'chmod 1777 /mnt/lxc-containers/tmp',
      'mount --bind /mnt/lxc-containers/tmp /tmp',
      'mkdir -p /var/lib/lxc',
      'mkdir -p /mnt/lxc-containers/lxc',
      'mount --bind /mnt/lxc-containers/lxc /var/lib/lxc',
      'echo "/dev/vg_lxc/lxc-containers /mnt/lxc-containers ext4 defaults 0 0" >> /etc/fstab',
      'echo "/mnt/lxc-containers/tmp /tmp none defaults,bind 0 0" >> /etc/fstab',
      'echo "/mnt/lxc-containers/lxc /var/lib/lxc none defaults,bind 0 0" >> /etc/fstab',
    ]
    command commands.join(' && ')
  end
end
