remote_file '/etc/apt/trusted.gpg.d/debci.gpg' do
  source 'files/debci.gpg.bin'
  owner 'root'
  group 'root'
  mode  '0644'
end

remote_file '/etc/apt/preferences.d/debci-pinning' do
  owner 'root'
  group 'root'
  mode  '0644'
end
template '/etc/apt/apt.conf.d/debci-default-release' do
  owner 'root'
  group 'root'
  mode  '0644'
end

template '/etc/apt/sources.list.d/debci.list' do
  variables mirror: node['mirror']
  notifies :run, 'execute[apt-get update]', :immediately
  owner 'root'
  group 'root'
  mode  '0644'
end

directory '/etc/debci/conf.d'
file "/etc/debci/conf.d/00_backend.conf" do
  content "debci_backend=#{node['debci']['backend'] || 'lxc'}\n"
  owner    'root'
  group   'root'
  mode    '0644'
end

if node['debci'] && node['debci']['suites']
  file '/etc/debci/conf.d/01_suite_list.conf' do
    content "debci_suite_list='%s'\n" % node['debci']['suites'].join(' ')
    owner    'root'
    group   'root'
    mode    '0644'
  end
end

if node['debci'] && node['debci']['arch']
  file '/etc/debci/conf.d/02_arch.conf' do
    content "debci_arch='%s'\n" % node['debci']['arch']
    owner    'root'
    group   'root'
    mode    '0644'
  end
end
