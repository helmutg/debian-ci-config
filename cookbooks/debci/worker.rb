package 'autodep8' do
  action :install
  version node['versions']['autodep8']
end
package 'autopkgtest' do
  action :install
  version node['versions']['autopkgtest']
end

package 'lxc'
package 'lxc-templates'

template '/etc/lxc/default.conf' do
  source  'templates/lxc.conf.erb'
end

package 'debci' do
  action :install
  version node["versions"]["debci"]
end

package 'debci-worker' do
  action :install
  version node["versions"]["debci"]
end
service 'debci-worker' do
  action :nothing
end

if node['debci']['amqp_server']
  [
    "/etc/debci/ca.crt",
    "/etc/debci/#{node["fqdn"]}.crt",
    "/etc/debci/#{node["fqdn"]}.key",
  ].each do |f|
    remote_file f do
      sensitive true
      source    "../rabbitmq/files/host-#{node[:fqdn]}/#{File.basename(f)}"
      owner     'root'
      group     'debci'
      mode      '0440'
    end
  end
  template '/etc/debci/conf.d/00_amqp_server.conf' do
    owner     'root'
    group     'debci'
    mode      '0440'
  end
end

if node['parallel_jobs']
  jobs = Integer(node['parallel_jobs'])
  (2..jobs).each do |i|
    execute "systemctl enable debci-worker@#{i}"
    execute "systemctl start debci-worker@#{i}"
  end
end

file '/etc/debci/conf.d/00_mirror.conf' do
  content [
    "export debci_mirror=#{node['mirror']}\n",
    "export MIRROR=#{node['mirror']}\n",
  ].join
  owner "root"
  group "root"
  mode  "0644"
end

if node['debci'] && node['debci']['timeout-factor']
  file '/etc/debci/conf.d/00_autopkgtest_options.conf' do
    content [
      "export debci_autopkgtest_args=--timeout-factor=#{node['debci']['timeout-factor']}\n",
    ].join
    owner "root"
    group "root"
    mode  "0644"
  end
end

include_recipe "on_debci_conf_change"
on_debci_conf_change 'debci-worker' do
  action :restart
end

[
  ['debci_packages_processed', :template],
  ['debci_packages_being_processed', :remote_file],
  ['debci_containers', :remote_file],
].each do |munin_plugin, type|
  send(type, "/etc/munin/plugins/#{munin_plugin}") do
    owner     'root'
    group     'debci'
    mode      '0755'
    notifies :restart, 'service[munin-node]'
  end
  file "/etc/munin/plugin-conf.d/#{munin_plugin}" do
    content ["[#{munin_plugin}]", 'user root'].join("\n") + "\n"
    notifies :restart, 'service[munin-node]'
  end
end

remote_file '/etc/cron.daily/cleanup-leftover-containers' do
  owner     'root'
  group     'root'
  mode      '0755'
end

remote_file '/usr/local/bin/prevent-disk-filling' do
  owner     'root'
  group     'root'
  mode      '0755'
end

remote_file '/etc/cron.d/prevent-disk-filling_cron_d' do
  owner     'root'
  group     'root'
  mode      '0644'
end
