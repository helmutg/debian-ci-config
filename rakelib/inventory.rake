desc "Prints server inventory"
task :inventory do
  header = ["Host", "Runs", "Location"]
  data = [header]
  Chake.nodes.each do |node|
    arch = node.data["debci"]["arch"]
    arch ||= File.basename(node.data["chake_metadata"]["definition_file"], ".yaml")
    location = node.data.dig("metadata", "location")
    data << [node.hostname, arch, location]
  end
  IO.popen(["column", "--table", "--separator=\t"], "w") do |table|
    data.each do |row|
      table.puts(row.join("\t"))
    end
  end
end
