$master_node = $nodes.find do |node|
  node.data.values_at('itamae', 'itamae-remote').flatten.include?('roles/ci_master.rb')
end
$workers = $nodes.select do |node|
  node.data.values_at('itamae', 'itamae-remote').flatten.include?('roles/ci_worker.rb')
end
