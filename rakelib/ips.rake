desc 'list public and private IPS'
task :ips => :load_ips do
  IO.popen(["column", "-t"], mode="w") do |io|
    $nodes.sort_by(&:hostname).each do |node|
      data = node.data
      io.puts sprintf("%s\t%s\t%s\n", node.hostname, data.fetch('public_ip', '-'), data.fetch('internal_ip', '-'))
    end
  end
end

task :load_ips do
  if $ENV == "development"
    $nodes.each do |node|
      node.data["internal_ip"] ||= `getent hosts #{node.hostname}`.split.first
      node.data["public_ip"] ||= node.data["internal_ip"]
    end
  end
  $nodes.each do |node|
    if node.data['internal_ip']
      node.data['hosts'] = $nodes.inject({}) { |memo,other| memo[other.hostname] = other.data['internal_ip'] || other.data['public_ip']; memo }
    else
      node.data['hosts'] = $nodes.inject({}) { |memo,other| memo[other.hostname] = other.data['public_ip']; memo }
    end
    node.data['security_hosts'] = $nodes.inject({}) do |memo, node|
      if node.data['debci']['enable_security']
        memo[node.hostname] = node.data['public_ip']
      end
      memo
    end
    node.data['mirror'] ||= 'http://deb.debian.org/debian'
  end
end

task :converge_common => :load_ips
