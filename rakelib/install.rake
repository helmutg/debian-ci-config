require_program "debchange", "devscripts"
require_program "dpkg-parsechangelog", "dpkg-dev"
require_program "gbp", "git-buildpackage"
require_program "git"


def install_target(t, args)
  if args[:to]
    "#{t}:#{args[:to]}"
  else
    t
  end
end

$DEBS_DIR = File.absolute_path(File.join(ENV["CHAKE_TMPDIR"], 'debs'))

Chake.nodes.each do |node|
  desc "Uploads built debci binaries to #{node.hostname}"
  multitask "debci:copy:#{node.hostname}" do
    dest = "#{node.username}@#{node.hostname}:/var/tmp/debs/"
    sh *node.rsync, "-avp", "--delete", "#{$DEBS_DIR}/", dest
  end
end

desc 'Uploads build debci binaries'
multitask 'debci:copy' => Chake.nodes.map { |node| "debci:copy:#{node.hostname}"}

desc 'Builds and uploads debci tree at $FROM (default: ../debci)'
multitask 'debci:upload' do |_, args|
  source = ENV.fetch('FROM', '../debci')
  mkdir_p $DEBS_DIR
  v = nil
  chdir source do
    debs = []
    v0 = `dpkg-parsechangelog -SVersion`.strip
    v = `git describe --tags | tr - +`.strip
    if v == v0
      debs = `debc --list-debs`.split
    end
    if debs.empty? || v != v0
      sh "debchange -v #{v} Snapshot"
      debs = `debc --list-debs`.split
      if debs.empty?
        sh 'DEB_BUILD_OPTIONS=nocheck gbp buildpackage --git-export=WC'
        debs = `debc --list-debs`.split
      end
      sh 'git checkout -- debian/changelog'
    end
    rm_f Dir[$DEBS_DIR + '/*.deb']
    cp debs, $DEBS_DIR
  end
  Rake::Task[install_target('debci:copy', args)].invoke
  Rake::Task[install_target('run', args)].invoke("sudo cp /var/tmp/debs/*.deb /srv/local-apt-repository/ && while ! grep -q '^Version: #{v}$' /var/lib/local-apt-repository/Packages; do sleep 1; done && sudo apt-get update")
  ENV["DEBCI_VERSION"] = v
end

desc 'Installs debci from source tree at $FROM (default: ../debci)'
multitask :install, :to do |_, args|
  Rake::Task['debci:upload'].invoke(args[:to])
  Rake::Task[install_target('converge', args)].invoke
end
