desc 'run a command on all workers'
multitask 'run:workers' => $workers.map { |w| "run:#{w.hostname}" }

namespace :workers do
  desc 'stop all workers'
  task :stop do
    Rake::Task[:run_input].invoke("sudo systemctl stop debci-worker@* ; sleep 5 ; sudo systemctl stop debci-worker")
    Rake::Task['run:workers'].invoke
  end

  desc 'start all workers'
  task :start do
    Rake::Task[:run_input].invoke("sudo systemctl start debci-worker")
    Rake::Task['run:workers'].invoke
  end

  desc 'get worker status'
  task :status do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --running); do printf \"$c\\t\"; cat /var/lib/lxc/$c/rootfs/var/tmp/debci.pkg 2>/dev/null || echo; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of autopkgtest-* lxc containers'
  task :lxc_age do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter autopkgtest-*); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of new autopkgtest lxc container'
  task :lxc_age_new do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter autopkgtest-*-new); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of ci-* lxc containers'
  task :lxc_age_ci do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter ci-*); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show how many debci-workers each node has'
  task :capacity do
    workers.each do |node|
      number = node.data["parallel_jobs"] || 1
      printf "%-20s\t%2d\n", node.hostname, number
     end
  end

  desc 'Runs `apt-get dist-upgrade` on all workers'
  task :dist_upgrade do
    Rake::Task['run'].invoke('sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get -qy -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold dist-upgrade')
#    Rake::Task['converge'].invoke
  end
end

desc 'show how many debci-workers each arch has'
task :capacity do
  data = workers.inject({}) do |memo, node|
    arch = node.data["debci"]["arch"]
    arch ||= node.hostname.sub(/ci-worker-(.*)-\d+/,  '\1')
    arch = "amd64" if arch =~ /^ci-worker/
    memo[arch] ||= 0
    memo[arch] += node.data["parallel_jobs"] || 1
    memo
  end
  data.each do |arch, count|
    printf "%-10s\t%2d\n", arch, count
  end
end


desc 'restarts armel-02 VM'
task :armel_02_restart do
  Rake::Task[:run_input].invoke("virsh destroy ci-runner-arm-01 && virsh start ci-runner-arm-01")
  Rake::Task["run:ci-worker-armel-01"].invoke
end
