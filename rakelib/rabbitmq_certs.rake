require_program 'make-cadir', 'easy-rsa'

CA_DIR = "config/#{$ENV}/ca"

directory CA_DIR do
  sh "make-cadir #{CA_DIR}"
  cd CA_DIR do
    sh './easyrsa init-pki'
    sh './easyrsa build-ca nopass'
    sh './easyrsa gen-dh'
    encrypt 'pki/private/ca.key'
  end
end

desc "Generate needed host certificates for rabbitmq"
task :rabbitmq_certs => CA_DIR do
  needed = $nodes.select do |node|
    key = "#{CA_DIR}/pki/private/#{node.hostname}.key"
    key += ".asc" if $ENV != "development"
    !File.exist?(key)
  end

  unless needed.empty?
    chdir CA_DIR do
      decrypt('pki/private/ca.key') do
        needed.each do |node|
          kind = node.hostname =~ /master/ ? 'server' : 'client'
          sh "./easyrsa build-#{kind}-full #{node.hostname} nopass"
          key = "pki/private/#{node.hostname}.key"
          if $ENV != "development"
            encrypt key
            key = key + ".asc"
          end
          target = "../../../cookbooks/rabbitmq/files/host-#{node.hostname}/"
          mkdir_p target
          cp "pki/issued/#{node.hostname}.crt", target
          cp key, target
          cp "pki/ca.crt", target
          cp "pki/dh.pem", target
        end
      end
    end
  end
end

task :converge_common => :rabbitmq_certs
