$VERSIONS = {
  "debci"         => ENV.fetch('DEBCI_VERSION', '3.6'),
  "autodep8"      => ENV.fetch('AUTODEP8_VERSION', '0.28'),
  'autopkgtest'   => ENV.fetch('AUTOPKGTEST_VERSION', '5.29'),
}
$nodes.each do |node|
  node.data['versions'] = $VERSIONS.dup
end
